/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class CutTransaction implements jTPS_Transaction {
    Pane pane;
    Node node;
    Node copiedNode;
    
    public CutTransaction(Pane pane, Node node, Node copiedNode){
        this.pane = pane;
        this.node = node;
        this.copiedNode = copiedNode;
    }

    @Override
    public void doTransaction() {
        pane.getChildren().remove(node);
    }

    @Override
    public void undoTransaction() {
        pane.getChildren().add(node);
        copiedNode = null;
    }
    
}
