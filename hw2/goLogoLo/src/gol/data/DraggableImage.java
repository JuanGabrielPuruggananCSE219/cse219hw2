/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.data;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Gabelybabe
 */
public class DraggableImage extends ImageView implements Draggable {
    String path;
    double startX;
    double startY;
    Image image;
    
    public DraggableImage(Image image, String path){
        super(image);
        setX(0.0);
        setY(0.0);
	startX = 0.0;
	startY = 0.0;
        this.path = path;
    }
    
    public String getPath(){
        return path;
    }

    @Override
    public golState getStartingState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void start(int x, int y) {
        startX = x;
	startY = y;
	setX(x);
	setY(y);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - getX();
	double diffY = y - getY();
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() {
        return 0.0;
    }

    @Override
    public double getHeight() {
        return 0.0;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
	yProperty().set(initY);
    }

    @Override
    public String getShapeType() {
        return IMAGE;
    }

    @Override
    public void startSelected(int x, int y) {
        startX = x;
        startY = y;
    }

    @Override
    public Node copy() {
        image = this.getImage();
        DraggableImage copiedImage = new DraggableImage(image, getPath());
        copiedImage.startX = this.startX;
        copiedImage.startY = this.startY;
        return copiedImage;
    }
    
}
