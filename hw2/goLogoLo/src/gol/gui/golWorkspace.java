package gol.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import static gol.golLanguageProperty.ELLIPSE_ICON;
import static gol.golLanguageProperty.ELLIPSE_TOOLTIP;
import static gol.golLanguageProperty.MOVE_TO_BACK_ICON;
import static gol.golLanguageProperty.MOVE_TO_BACK_TOOLTIP;
import static gol.golLanguageProperty.MOVE_TO_FRONT_ICON;
import static gol.golLanguageProperty.MOVE_TO_FRONT_TOOLTIP;
import static gol.golLanguageProperty.RECTANGLE_ICON;
import static gol.golLanguageProperty.RECTANGLE_TOOLTIP;
import static gol.golLanguageProperty.REMOVE_ICON;
import static gol.golLanguageProperty.REMOVE_TOOLTIP;
import static gol.golLanguageProperty.SELECTION_TOOL_ICON;
import static gol.golLanguageProperty.SELECTION_TOOL_TOOLTIP;
import static gol.golLanguageProperty.SNAPSHOT_ICON;
import static gol.golLanguageProperty.SNAPSHOT_TOOLTIP;
import gol.data.golData;
import static gol.data.golData.BLACK_HEX;
import static gol.data.golData.WHITE_HEX;
import gol.data.golState;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static gol.css.golStyle.*;
import gol.data.DraggableText;
import static gol.golLanguageProperty.ADD_IMAGE_ICON;
import static gol.golLanguageProperty.ADD_IMAGE_TOOLTIP;
import static gol.golLanguageProperty.ADD_TEXT_ICON;
import static gol.golLanguageProperty.ADD_TEXT_TOOLTIP;
import static gol.golLanguageProperty.BOLD_ICON;
import static gol.golLanguageProperty.BOLD_TOOLTIP;
import static gol.golLanguageProperty.ITALIC_ICON;
import static gol.golLanguageProperty.ITALIC_TOOLTIP;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import jtps.jTPS;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Juan Gabriel Purugganan
 * @version 1.0
 */
public class golWorkspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // HAS ALL THE CONTROLS FOR EDITING
    VBox editToolbar;
    
    // FIRST ROW
    HBox row1Box;
    Button selectionToolButton;
    Button removeButton;
    Button rectButton;
    Button ellipseButton;
    
    // NEW ROW
    HBox rowNewBox;
    Button addImageButton;
    Button addTextButton;
    
    // Row for Text styles
    HBox rowForTextBox;
    ComboBox fontFamilyBox;
    ComboBox fontSizeBox;
    Button boldButton;
    Button italicButton;
    
    // SECOND ROW
    HBox row2Box;
    Button moveToBackButton;
    Button moveToFrontButton;

    // THIRD ROW
    VBox row3Box;
    Label backgroundColorLabel;
    ColorPicker backgroundColorPicker;

    // FORTH ROW
    VBox row4Box;
    Label fillColorLabel;
    ColorPicker fillColorPicker;
    
    // FIFTH ROW
    VBox row5Box;
    Label outlineColorLabel;
    ColorPicker outlineColorPicker;
        
    // SIXTH ROW
    VBox row6Box;
    Label outlineThicknessLabel;
    Slider outlineThicknessSlider;
    
    // SEVENTH ROW
    HBox row7Box;
    Button snapshotButton;
    
    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;
    
    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    LogoEditController logoEditController;    

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    static jTPS jTPS = new jTPS();
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public golWorkspace(AppTemplate initApp) {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        

        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();    
    }
    
    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    
    public ColorPicker getFillColorPicker() {
	return fillColorPicker;
    }
    
    public ColorPicker getOutlineColorPicker() {
	return outlineColorPicker;
    }
    
    public ColorPicker getBackgroundColorPicker() {
	return backgroundColorPicker;
    }
    
    public Slider getOutlineThicknessSlider() {
	return outlineThicknessSlider;
    }
    
    public ComboBox getFontSizeBox(){
        return fontSizeBox;
    }

    public Pane getCanvas() {
	return canvas;
    }
    
    public jTPS getjTPS(){
        return jTPS;
    }
    
    public ComboBox getFontFamilyBox(){
        return fontFamilyBox;
    }
        
    // HELPER SETUP METHOD
    private void initLayout() {
	// THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
	editToolbar = new VBox();
	
	// ROW 1
	row1Box = new HBox();
	selectionToolButton = gui.initChildButton(row1Box, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), true);
	removeButton = gui.initChildButton(row1Box, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), true);
	rectButton = gui.initChildButton(row1Box, RECTANGLE_ICON.toString(), RECTANGLE_TOOLTIP.toString(), false);
	ellipseButton = gui.initChildButton(row1Box, ELLIPSE_ICON.toString(), ELLIPSE_TOOLTIP.toString(), false);
        
        // NEW ROW
        rowNewBox = new HBox();
        addImageButton = gui.initChildButton(rowNewBox, ADD_IMAGE_ICON.toString(), ADD_IMAGE_TOOLTIP.toString(), true);
        addTextButton = gui.initChildButton(rowNewBox, ADD_TEXT_ICON.toString(), ADD_TEXT_TOOLTIP.toString(), true);
        
        // ROW FOR TEXT
        rowForTextBox = new HBox();
        
        ObservableList<String> families = 
        FXCollections.observableArrayList(
            "Palatino Linotype",
            "Cambria",
            "Arial",
            "Times New Roman",
            "Courier"
        );
        fontFamilyBox = new ComboBox(FXCollections.observableArrayList(Font.getFamilies()));
        fontFamilyBox.setValue(fontFamilyBox.getItems().get(0));
        ObservableList<Integer> sizes = 
        FXCollections.observableArrayList(
            8,
            9,
            10,
            11,
            12,
            14,
            16,
            18,
            20,
            22,
            24,
            26,
            28,
            32,
            36,
            44,
            48,
            56,
            62,
            78
        );
        fontSizeBox = new ComboBox(sizes);
        rowForTextBox.getChildren().addAll(fontFamilyBox, fontSizeBox);
        boldButton = gui.initChildButton(rowForTextBox, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), true);
        italicButton = gui.initChildButton(rowForTextBox, ITALIC_ICON.toString(), ITALIC_TOOLTIP.toString(), true);

	// ROW 2
	row2Box = new HBox();
	moveToBackButton = gui.initChildButton(row2Box, MOVE_TO_BACK_ICON.toString(), MOVE_TO_BACK_TOOLTIP.toString(), true);
	moveToFrontButton = gui.initChildButton(row2Box, MOVE_TO_FRONT_ICON.toString(), MOVE_TO_FRONT_TOOLTIP.toString(), true);

	// ROW 3
	row3Box = new VBox();
	backgroundColorLabel = new Label("Background Color");
	backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
	row3Box.getChildren().add(backgroundColorLabel);
	row3Box.getChildren().add(backgroundColorPicker);

	// ROW 4
	row4Box = new VBox();
	fillColorLabel = new Label("Fill Color");
	fillColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
	row4Box.getChildren().add(fillColorLabel);
	row4Box.getChildren().add(fillColorPicker);
	
	// ROW 5
	row5Box = new VBox();
	outlineColorLabel = new Label("Outline Color");
	outlineColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
	row5Box.getChildren().add(outlineColorLabel);
	row5Box.getChildren().add(outlineColorPicker);
	
	// ROW 6
	row6Box = new VBox();
	outlineThicknessLabel = new Label("Outline Thickness");
	outlineThicknessSlider = new Slider(0, 10, 1);
	row6Box.getChildren().add(outlineThicknessLabel);
	row6Box.getChildren().add(outlineThicknessSlider);
	
	// ROW 7
	row7Box = new HBox();
	snapshotButton = gui.initChildButton(row7Box, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);
	
	// NOW ORGANIZE THE EDIT TOOLBAR
	editToolbar.getChildren().add(row1Box);
        editToolbar.getChildren().add(rowNewBox);
        editToolbar.getChildren().add(rowForTextBox);
	editToolbar.getChildren().add(row2Box);
	editToolbar.getChildren().add(row3Box);
	editToolbar.getChildren().add(row4Box);
	editToolbar.getChildren().add(row5Box);
	editToolbar.getChildren().add(row6Box);
	editToolbar.getChildren().add(row7Box);
	
	// WE'LL RENDER OUR STUFF HERE IN THE CANVAS
	canvas = new Pane();
	debugText = new Text();
	canvas.getChildren().add(debugText);
	debugText.setX(100);
	debugText.setY(100);
	
	// AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
	golData data = (golData)app.getDataComponent();
	data.setShapes(canvas.getChildren());

	// AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(editToolbar);
	((BorderPane)workspace).setCenter(canvas);
    }
    
    // HELPER SETUP METHOD
    private void initControllers() {
        golData data = (golData)app.getDataComponent();
        Node shape = data.getSelectedShape();
	// MAKE THE EDIT CONTROLLER
	logoEditController = new LogoEditController(app);
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
	selectionToolButton.setOnAction(e->{
	    logoEditController.processSelectSelectionTool();
	});
	removeButton.setOnAction(e->{
	    logoEditController.processRemoveSelectedShape();
	});
	rectButton.setOnAction(e->{
	    logoEditController.processSelectRectangleToDraw();
	});
	ellipseButton.setOnAction(e->{
	    logoEditController.processSelectEllipseToDraw();
	});
	
	moveToBackButton.setOnAction(e->{
	    logoEditController.processMoveSelectedShapeToBack();
	});
	moveToFrontButton.setOnAction(e->{
	    logoEditController.processMoveSelectedShapeToFront();
	});

	backgroundColorPicker.setOnAction(e->{
	    logoEditController.processSelectBackgroundColor();
	});
	fillColorPicker.setOnAction(e->{ 
	    logoEditController.processSelectFillColor();
	});
	outlineColorPicker.setOnAction(e->{
	    logoEditController.processSelectOutlineColor();
	});
	outlineThicknessSlider.valueProperty().addListener(e-> {
	    logoEditController.processSelectOutlineThickness();
	});
	snapshotButton.setOnAction(e->{
	    logoEditController.processSnapshot();
	});
        addImageButton.setOnAction(e->{
            logoEditController.processAddImage();
        });
        addTextButton.setOnAction(e->{
            logoEditController.processAddText();
        });
	
	// MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
	canvas.setOnMousePressed(e->{
            if(e.getClickCount() == 2){
                canvasController.processDoubleClick((int)e.getX(), (int)e.getY());
            }
            else{
                canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
            }
	    
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	});
        
        boldButton.setOnAction(e-> {
            canvasController.setBold();
        });
        italicButton.setOnAction(e->{
            canvasController.setItalic();
        });
        
        fontFamilyBox.setOnAction(e-> {
            canvasController.changeFontFamily(fontFamilyBox.getValue().toString());
        });
        
        fontSizeBox.setOnAction(e -> {
            canvasController.changeFontSize(fontSizeBox.getValue().toString());
        });
    }
    
    public String getFilePath(){
        return canvasController.getFilePath();
    }

    // HELPER METHOD
    public void loadSelectedShapeSettings(Shape shape) {
	if (shape != null) {
	    Color fillColor = (Color)shape.getFill();
	    Color strokeColor = (Color)shape.getStroke();
	    double lineThickness = shape.getStrokeWidth();
	    fillColorPicker.setValue(fillColor);
	    outlineColorPicker.setValue(strokeColor);
	    outlineThicknessSlider.setValue(lineThickness);	    
	}
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
	canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
	
	// COLOR PICKER STYLE
	fillColorPicker.getStyleClass().add(CLASS_BUTTON);
	outlineColorPicker.getStyleClass().add(CLASS_BUTTON);
	backgroundColorPicker.getStyleClass().add(CLASS_BUTTON);
	
	editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
	row1Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        rowNewBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        rowForTextBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	row2Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	row3Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	backgroundColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	
	row4Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	fillColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	outlineColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row6Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	outlineThicknessLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row7Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
    }

    /**
     * This function reloads all the controls for editing logos
     * the workspace.
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
	golData dataManager = (golData)data;
        canvasController = new CanvasController(app);
	if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(true);
	    ellipseButton.setDisable(false);
	}
	else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(true);
	}
        else if(dataManager.isInState(golState.STARTING_IMAGE)){
            try {
                canvasController.addImageToCanvas(0, 0);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(golWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(dataManager.isInState(golState.STARTING_TEXT)){
            canvasController.addTextToCanvas(0, 0);
        }
	else if (dataManager.isInState(golState.SELECTING_SHAPE) 
		|| dataManager.isInState(golState.DRAGGING_SHAPE)
		|| dataManager.isInState(golState.DRAGGING_NOTHING)) {
	    boolean shapeIsNotSelected = dataManager.getSelectedShape() == null;
	    selectionToolButton.setDisable(true);
	    removeButton.setDisable(shapeIsNotSelected);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(false);
            addImageButton.setDisable(false);
            addTextButton.setDisable(false);
            boldButton.setDisable(false);
            italicButton.setDisable(false);
	    moveToFrontButton.setDisable(shapeIsNotSelected);
	    moveToBackButton.setDisable(shapeIsNotSelected);
	}
	
	removeButton.setDisable(dataManager.getSelectedShape() == null);
	backgroundColorPicker.setValue(dataManager.getBackgroundColor());
    }
    
    @Override
    public void resetWorkspace() {
        // WE ARE NOT USING THIS, THOUGH YOU MAY IF YOU LIKE
    }

    @Override
    public void changeTooltipLanguage() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Tooltip selectionToolTooltip = new Tooltip(props.getProperty(SELECTION_TOOL_TOOLTIP.toString()));
        Tooltip removeTooltip = new Tooltip(props.getProperty(REMOVE_TOOLTIP.toString()));
        Tooltip rectangleTooltip = new Tooltip(props.getProperty(RECTANGLE_TOOLTIP.toString()));
        Tooltip ellipseTooltip = new Tooltip(props.getProperty(ELLIPSE_TOOLTIP.toString()));
        Tooltip moveToBackTooltip = new Tooltip(props.getProperty(MOVE_TO_BACK_TOOLTIP.toString()));
        Tooltip moveToFrontTooltip = new Tooltip(props.getProperty(MOVE_TO_FRONT_TOOLTIP.toString()));
        Tooltip snapshotTooltip = new Tooltip(props.getProperty(SNAPSHOT_TOOLTIP.toString()));
        Tooltip addImageTooltip = new Tooltip(props.getProperty(ADD_IMAGE_TOOLTIP.toString()));
        Tooltip addTextTooltip = new Tooltip(props.getProperty(ADD_TEXT_TOOLTIP.toString()));
        Tooltip boldTooltip = new Tooltip(props.getProperty(BOLD_TOOLTIP.toString()));
        Tooltip italicTooltip = new Tooltip(props.getProperty(ITALIC_TOOLTIP.toString()));
        
        selectionToolButton.setTooltip(selectionToolTooltip);
        removeButton.setTooltip(removeTooltip);
        rectButton.setTooltip(rectangleTooltip);
        ellipseButton.setTooltip(ellipseTooltip);
        moveToBackButton.setTooltip(moveToBackTooltip);
        moveToFrontButton.setTooltip(moveToFrontTooltip);
        snapshotButton.setTooltip(snapshotTooltip);
        addImageButton.setTooltip(addImageTooltip);
        addTextButton.setTooltip(addTextTooltip);
        boldButton.setTooltip(boldTooltip);
        italicButton.setTooltip(italicTooltip);
        
    }

    @Override
    public void copyShape() {
        canvasController.processCopy();
    }

    @Override
    public void pasteShape() {
        canvasController.processShape();
    }
    
    @Override
    public void cutShape(){
        canvasController.processCut();
    }

    @Override
    public void undo() {
        jTPS.undoTransaction();
    }

    @Override
    public void redo() {
        jTPS.doTransaction();
    }
}