/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.data;

import javafx.scene.Node;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Gabelybabe
 */
public class DraggableText extends Text implements Draggable {
    double startX;
    double startY;
    String s;
    public DraggableText(String s){
        super(s);
        this.s = s;
    }

    @Override
    public golState getStartingState() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void start(int x, int y) {
        startX = x;
	startY = y;
	setX(x);
	setY(y);
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	xProperty().set(newX);
	yProperty().set(newY);
	startX = x;
	startY = y;
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
	yProperty().set(initY);
    }

    @Override
    public String getShapeType() {
        return TEXT;
    }

    @Override
    public double getWidth() {
        return 0.0;
    }

    @Override
    public double getHeight() {
        return 0.0;
    }

    @Override
    public void startSelected(int x, int y) {
        startX = x;
        startY = y;
    }

    @Override
    public Node copy() {
        s = this.getText();
        DraggableText copiedText = new DraggableText(s);
        copiedText.startX = this.startX;
        copiedText.startY = this.startY;
        if(this.getFont().getStyle().equals("Bold"))
            copiedText.setFont(Font.font(this.getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, this.getFont().getSize()));
        else if(this.getFont().getStyle().equals("Italic"))
            copiedText.setFont(Font.font(this.getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, this.getFont().getSize()));
        else if(this.getFont().getStyle().equals("Bold Italic"))
            copiedText.setFont(Font.font(this.getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, this.getFont().getSize()));
        else if(this.getFont().getStyle().equals("Regular"))
            copiedText.setFont(Font.font(this.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, this.getFont().getSize()));
//        copiedText.setFont(Font.font(this.getFont().getFamily(), this.getFont().getSize()));
        copiedText.setStyle(this.getStyle());
        copiedText.setFill(this.getFill());
        copiedText.setStroke(this.getStroke());
        copiedText.setStrokeWidth(this.getStrokeWidth());
        return copiedText;
        
    }
    
}
