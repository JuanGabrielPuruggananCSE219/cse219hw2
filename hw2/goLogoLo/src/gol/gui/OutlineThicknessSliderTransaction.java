/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.golData;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class OutlineThicknessSliderTransaction implements jTPS_Transaction {
    golData dataManager;
    golWorkspace workspace;
    int outlineThickness;
    int oldOutlineThickness;
    
    public OutlineThicknessSliderTransaction(golData dataManager, golWorkspace workspace, int outlineThickness, int oldOutlineThickness){
        this.dataManager = dataManager;
        this.workspace = workspace;
        this.outlineThickness = outlineThickness;
        this.oldOutlineThickness = oldOutlineThickness;
    }

    @Override
    public void doTransaction() {
        dataManager.setCurrentOutlineThickness(outlineThickness);
    }

    @Override
    public void undoTransaction() {
        dataManager.setCurrentOutlineThickness(oldOutlineThickness);
    }
    
}
