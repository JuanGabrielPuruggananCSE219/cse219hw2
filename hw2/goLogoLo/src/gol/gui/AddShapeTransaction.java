/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class AddShapeTransaction implements jTPS_Transaction {
    ObservableList<Node> shapes;
    Node shape;
    
    public AddShapeTransaction(ObservableList<Node> shapes, Node shape){
        this.shapes = shapes;
        this.shape = shape;
    }

    @Override
    public void doTransaction() {
        shapes.add(shape);
    }

    @Override
    public void undoTransaction() {
        shapes.remove(shape);
    }
    
}
