/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.Node;
import javafx.scene.text.Font;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class ChangeFontFamilyTransaction implements jTPS_Transaction {
    String family;
    String currentFamily;
    golData dataManager;
    golWorkspace workspace;
    Node node;
    
    public ChangeFontFamilyTransaction(String family, String currentFamily, golData dataManager, golWorkspace workspace, Node node){
        this.family = family;
        this.currentFamily = currentFamily;
        this.dataManager = dataManager;
        this.workspace = workspace;
        this.node = node;
    }

    @Override
    public void doTransaction() {
        workspace.getFontFamilyBox().setValue(family);
        ((DraggableText) node).setFont(Font.font(family,((DraggableText) node).getFont().getSize()));
        
    }

    @Override
    public void undoTransaction() {
        workspace.getFontFamilyBox().setValue(currentFamily);
        ((DraggableText) node).setFont(Font.font(currentFamily,((DraggableText) node).getFont().getSize()));
    }
    
}
