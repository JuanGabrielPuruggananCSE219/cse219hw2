/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class AddTextTransaction implements jTPS_Transaction {
    Pane pane;
    Text text;
    
    public AddTextTransaction(Pane pane, Text text){
        this.pane = pane;
        this.text = text;
    }

    @Override
    public void doTransaction() {
        pane.getChildren().add(text);
    }

    @Override
    public void undoTransaction() {
        pane.getChildren().remove(text);
    }
    
}
