/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.golData;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class FillColorTransaction implements jTPS_Transaction {
    golData dataManager;
    golWorkspace workspace;
    Color selectedColor;
    Color lastSelectedColor;
    
    public FillColorTransaction(golData dataManager, golWorkspace workspace, Color selectedColor, Color lastSelectedColor){
        this.dataManager = dataManager;
        this.workspace = workspace;
        this.selectedColor = selectedColor;
        this.lastSelectedColor = lastSelectedColor;
    }
    

    @Override
    public void doTransaction() {
        dataManager.setCurrentFillColor(selectedColor);
        workspace.getFillColorPicker().setValue(selectedColor);
    }

    @Override
    public void undoTransaction() {
        dataManager.setCurrentFillColor(lastSelectedColor);
        workspace.getFillColorPicker().setValue(lastSelectedColor);
    }
    
}
