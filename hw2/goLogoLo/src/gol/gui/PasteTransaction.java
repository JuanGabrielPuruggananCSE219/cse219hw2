/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class PasteTransaction implements jTPS_Transaction {
    Pane pane;
    Node node;
    
    public PasteTransaction(Pane pane, Node node){
        this.pane = pane;
        this.node = node;
    }
    

    @Override
    public void doTransaction() {
        pane.getChildren().add(node);
    }

    @Override
    public void undoTransaction() {
        pane.getChildren().remove(node);
    }
    
}
