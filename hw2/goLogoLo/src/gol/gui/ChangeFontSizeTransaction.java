/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.Node;
import javafx.scene.text.Font;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class ChangeFontSizeTransaction implements jTPS_Transaction {
    int size;
    int currentSize;
    golData dataManager;
    golWorkspace workspace;
    Node shape;
    
    public ChangeFontSizeTransaction(int size, int currentSize, golData dataManager, golWorkspace workspace, Node shape){
        this.size = size;
        this.currentSize = currentSize;
        this.dataManager = dataManager; 
        this.workspace = workspace; 
        this.shape = shape;
    }

    @Override
    public void doTransaction() {
        ((DraggableText) shape).setFont(Font.font(((DraggableText) shape).getFont().getFamily(), size));
        workspace.getFontSizeBox().setValue(size);
    }

    @Override
    public void undoTransaction() {
        ((DraggableText) shape).setFont(Font.font(((DraggableText) shape).getFont().getFamily(), currentSize));
        workspace.getFontSizeBox().setValue(currentSize);
    }
    
}
