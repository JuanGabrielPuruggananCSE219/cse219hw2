/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.DraggableImage;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class AddImageTransaction implements jTPS_Transaction {
    Pane pane;
    DraggableImage image;
    
    public AddImageTransaction(Pane pane, DraggableImage image){
        this.pane = pane;
        this.image = image;
    }

    @Override
    public void doTransaction() {
        pane.getChildren().add(image);
    }

    @Override
    public void undoTransaction() {
        pane.getChildren().remove(image);
    }
    
}
