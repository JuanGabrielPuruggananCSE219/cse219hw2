package gol.gui;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;
import gol.data.golData;
import gol.data.Draggable;
import gol.data.golState;
import static gol.data.golState.DRAGGING_NOTHING;
import static gol.data.golState.DRAGGING_SHAPE;
import static gol.data.golState.SELECTING_SHAPE;
import static gol.data.golState.SIZING_SHAPE;
import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableImage;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import static gol.data.golState.STARTING_IMAGE;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author Richard McKenna
 * @author Juan Gabriel Purugganan
 * @version 1.0
 */
public class CanvasController {

    AppTemplate app;
    Image image;
    ImageView imageView;
    Pane pane;
    boolean textAdded;
    DraggableText text;
    DraggableImage dragImage;
    boolean bold = false;
    boolean italic = false;
    Node copiedNode = null;
    double startX; 
    double startY;
    double endX;
    double endY;
    String filePath;

    public CanvasController(AppTemplate initApp) {
        textAdded = false;
        app = initApp;
    }
    
    public void addTextToCanvas(int x, int y){
        golData dataManager = (golData) app.getDataComponent();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        if(dataManager.isInState(golState.STARTING_TEXT)){
            TextInputDialog dialog = new TextInputDialog("");
            dialog.setTitle("Add Text to Canvas");
            dialog.setHeaderText("Text Input Dialog");
            dialog.setContentText("Please enter desired text: ");
            
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> System.out.println("Your name: " + name));
            text = new DraggableText(result.get());
            Text t = new Text(result.get());
            t.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){
                        System.out.println("Double clicked");
                    }
                }
            }
            });
            pane = workspace.getCanvas();
            StackPane movablePane = new StackPane();
 //           movablePane.getChildren().add(t);
            workspace.getjTPS().addTransaction(new AddTextTransaction(pane, text));
//            enableDragging(movablePane);
            textAdded = true;
            dataManager.setState(golState.SELECTING_SHAPE);
        }
    }

/*    
     private void enableDragging(Node node) {
        final ObjectProperty<Point2D> mouseAnchor = new SimpleObjectProperty<>();
        node.setOnMousePressed( event -> mouseAnchor.set(new Point2D(event.getSceneX(), event.getSceneY())));
        node.setOnMouseDragged( event -> {
            double deltaX = event.getSceneX() - mouseAnchor.get().getX();
            double deltaY = event.getSceneY() - mouseAnchor.get().getY();
            node.relocate(node.getLayoutX()+deltaX, node.getLayoutY()+deltaY);
            mouseAnchor.set(new Point2D(event.getSceneX(), event.getSceneY()));;
        });
    }
**/    

    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void addImageToCanvas(int x, int y) throws FileNotFoundException{
        golData dataManager = (golData) app.getDataComponent();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        if(dataManager.isInState(golState.STARTING_IMAGE)){
            Desktop desktop = Desktop.getDesktop();
            final FileChooser fileChooser = new FileChooser();
            FileInputStream input = null;
            Stage stage = new Stage();
            File file = fileChooser.showOpenDialog(stage);
            if (file != null){
                input = new FileInputStream(file.getAbsolutePath());
            }
            filePath = file.getAbsolutePath();
            image = new Image(input);
            dragImage = new DraggableImage(image, filePath);
            dragImage.startSelected(x, y);
            pane = workspace.getCanvas();
            Pane paneForImage = new Pane();
            paneForImage.getChildren().add(dragImage);
            workspace.getjTPS().addTransaction(new AddImageTransaction(pane, dragImage));
            dataManager.setState(golState.SELECTING_SHAPE);
        }
    }
    
    public String getFilePath(){
        return filePath;
    }
    
    public void processCanvasMousePress(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            Node shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            // AND START DRAGGING IT
            if (shape != null) {
                startX = ((Draggable)shape).getX();
                startY = ((Draggable)shape).getY();
                scene.setCursor(Cursor.MOVE);
                dataManager.setState(golState.DRAGGING_SHAPE);
                app.getGUI().updateToolbarControls(false);
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        } else if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
            dataManager.startNewRectangle(x, y);
        } else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
            dataManager.startNewEllipse(x, y);
        }
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    
    public void processDoubleClick(int x, int y) throws NoSuchElementException{
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            Node shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            
            if (shape != null) {
                if(shape instanceof DraggableText){
                    TextInputDialog dialog = new TextInputDialog(((DraggableText) shape).getText());
                    dialog.setTitle("Change Text on Canvas");
                    dialog.setHeaderText("Text Input Dialog");
                    dialog.setContentText("Please enter desired text: ");
            
                    Optional<String> result = dialog.showAndWait();
                    ((DraggableText) shape).setText(result.get());
                }
            }
        }
    }
    
    public void setBold(){
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.getSelectedShape();
            Scene scene = app.getGUI().getPrimaryScene();
            
            if (shape != null) {
                if(shape instanceof DraggableText){
                    if(bold == false){
                        ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, ((DraggableText)shape).getFont().getSize()));
                        bold = true;
                        if(italic == true)
                            ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, ((DraggableText)shape).getFont().getSize()));
                        
                    }
                    else{
                        ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, ((DraggableText)shape).getFont().getSize()));
                        bold = false;
                        if(italic == true)
                            ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, ((DraggableText)shape).getFont().getSize()));
                    }         
                }
            }
        }
    }
    
    public void setItalic(){
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.getSelectedShape();
            Scene scene = app.getGUI().getPrimaryScene();
            if (shape != null) {
                if(shape instanceof DraggableText){
                    if(italic == false){
                        ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, ((DraggableText)shape).getFont().getSize()));
                        italic = true;
                        if(bold == true)
                            ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, ((DraggableText)shape).getFont().getSize()));
                    }
                    else{
                        ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, ((DraggableText)shape).getFont().getSize()));
                        italic = false;
                        if(bold == true)
                            ((DraggableText)shape).setFont(Font.font(((DraggableText)shape).getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, ((DraggableText)shape).getFont().getSize()));
                    }
  //                  Font italic = Font.font(((DraggableText) shape).getFont(), FontPosture.ITALIC, ((DraggableText) shape).getFont().getSize());
  //                  ((DraggableText)shape).setFont(Font.font(((DraggableText) shape).getFont().getFamily(),((DraggableText) shape).getFont().getSize(), FontPosture.ITALIC));
                }
            }
        }
    }
    
    public void changeFontFamily(String family){
        golData dataManager = (golData) app.getDataComponent();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        String currentFamily = workspace.getFontFamilyBox().getValue().toString();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.getSelectedShape();
            Scene scene = app.getGUI().getPrimaryScene();
            if (shape != null) {
                if(shape instanceof DraggableText){
//                    ((DraggableText) shape).setFont(Font.font(family,((DraggableText) shape).getFont().getSize() ));
                    workspace.getjTPS().addTransaction(new ChangeFontFamilyTransaction(family, currentFamily, dataManager, workspace, shape));
                }
            }
        }
    }
    
    public void changeFontSize(String size){
        golData dataManager = (golData) app.getDataComponent();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.getSelectedShape();
            Scene scene = app.getGUI().getPrimaryScene();
            int currentSize = (Integer)workspace.getFontSizeBox().getValue();
            if (shape != null) {
                if(shape instanceof DraggableText){
//                    ((DraggableText) shape).setFont(Font.font(((DraggableText) shape).getFont().getFamily(), Integer.valueOf(size)));
                    workspace.getjTPS().addTransaction(new ChangeFontSizeTransaction(Integer.valueOf(size), currentSize, dataManager, workspace, shape));
                }
            }
        }
    }
    
    public void processCopy(){
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.getSelectedShape();
            Scene scene = app.getGUI().getPrimaryScene();
            if (shape != null) {
                if(shape instanceof DraggableImage){
                    copiedNode = ((DraggableImage)shape).copy();
                    System.out.println(copiedNode.toString());
                }
                else if(shape instanceof DraggableText){
                    copiedNode = ((DraggableText)shape).copy();
                    System.out.println(copiedNode.toString());
                }
                else if(shape instanceof DraggableRectangle){
                    copiedNode = ((DraggableRectangle)shape).copy();
                }
                else if(shape instanceof DraggableEllipse)
                    copiedNode = ((DraggableEllipse)shape).copy();
            }
        }
    }
    
    public void processShape(){
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        pane = workspace.getCanvas();
        if(copiedNode != null)
            workspace.getjTPS().addTransaction(new PasteTransaction(pane, copiedNode));
    }
    
    public void processCut(){
        golData dataManager = (golData) app.getDataComponent();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        pane = workspace.getCanvas();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.getSelectedShape();
            Scene scene = app.getGUI().getPrimaryScene();
            if (shape != null) {
                if(shape instanceof DraggableImage){
                    copiedNode = ((DraggableImage)shape).copy();
                    workspace.getjTPS().addTransaction(new CutTransaction(pane, shape, copiedNode));
                    System.out.println(copiedNode.toString());
                }
                else if(shape instanceof DraggableText){
                    copiedNode = ((DraggableText)shape).copy();
                    workspace.getjTPS().addTransaction(new CutTransaction(pane, shape, copiedNode));
                    System.out.println(copiedNode.toString());
                }
                else if(shape instanceof DraggableRectangle){
                    copiedNode = ((DraggableRectangle)shape).copy();
                    workspace.getjTPS().addTransaction(new CutTransaction(pane, shape, copiedNode));
                }
                else if(shape instanceof DraggableEllipse){
                    copiedNode = ((DraggableEllipse)shape).copy();
                    workspace.getjTPS().addTransaction(new CutTransaction(pane, shape, copiedNode));
                }
            }
        }
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseDragged(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            Draggable newDraggableShape = (Draggable) dataManager.getNewShape();
            newDraggableShape.size(x, y);
        } else if (dataManager.isInState(DRAGGING_SHAPE)) {
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            selectedDraggableShape.drag(x, y);
   //         endX = selectedDraggableShape.getX();
   //         endY = selectedDraggableShape.getY();
 //           workspace.getjTPS().addTransaction(new DragShapeTransaction(selectedDraggableShape, x, y, startX, startY, endX, endY, selectedDraggableShape.getWidth(), selectedDraggableShape.getHeight()));
            app.getGUI().updateToolbarControls(false);
        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseRelease(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            dataManager.selectSizedShape();
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(golState.DRAGGING_SHAPE)) {
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(golState.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_SHAPE);
        }
    }
}
