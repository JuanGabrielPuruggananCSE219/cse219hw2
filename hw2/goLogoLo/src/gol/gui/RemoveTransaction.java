/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.Draggable;
import gol.data.golData;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class RemoveTransaction implements jTPS_Transaction {
    golData dataManager;
    golWorkspace workspace;
    Pane pane;
    Node shape;
    
    public RemoveTransaction(golData dataManager, golWorkspace workspace, Pane pane, Node shape){
        this.dataManager = dataManager;
        this.workspace = workspace;
        this.pane = pane;
        this.shape = shape;
    }

    @Override
    public void doTransaction() {
        pane.getChildren().remove(shape);
        workspace.reloadWorkspace(dataManager);
    }

    @Override
    public void undoTransaction() {
        pane.getChildren().add(shape);
        ((Draggable)shape).setLocationAndSize(((Draggable)shape).getX(), ((Draggable)shape).getY(), ((Draggable)shape).getWidth(), ((Draggable)shape).getHeight());
        workspace.reloadWorkspace(dataManager);
    }
    
}
