package gol;

/**
 * This class provides the properties that are needed to be loaded from
 * language-dependent XML files.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public enum golLanguageProperty {
    SELECTION_TOOL_ICON,
    SELECTION_TOOL_TOOLTIP,
    
    ADD_IMAGE_ICON,
    ADD_IMAGE_TOOLTIP,
    
    ADD_TEXT_ICON,
    ADD_TEXT_TOOLTIP,
    
    FONT_FAMILY_ICON,
    FONT_FAMILY_TOOLTIP,
    
    FONT_SIZE_ICON,
    FONT_SIZE_TOOLTIP,
    
    BOLD_ICON,
    BOLD_TOOLTIP,
    
    ITALIC_ICON,
    ITALIC_TOOLTIP,
    
    REMOVE_ICON,
    REMOVE_TOOLTIP,
    
    RECTANGLE_ICON,
    RECTANGLE_TOOLTIP,
    
    ELLIPSE_ICON,
    ELLIPSE_TOOLTIP,
    
    MOVE_TO_BACK_ICON,
    MOVE_TO_BACK_TOOLTIP,
    MOVE_TO_FRONT_ICON,
    MOVE_TO_FRONT_TOOLTIP,
    
    BACKGROUND_TOOLTIP,
    FILL_TOOLTIP,
    OUTLINE_TOOLTIP,
    
    SNAPSHOT_ICON,
    SNAPSHOT_TOOLTIP,
}