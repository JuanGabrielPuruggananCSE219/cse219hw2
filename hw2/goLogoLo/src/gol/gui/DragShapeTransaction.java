/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import gol.data.Draggable;
import gol.data.DraggableRectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author Gabelybabe
 */
public class DragShapeTransaction implements jTPS_Transaction {
    Draggable selectedDraggableShape;
    int x;
    int y;
    double startX;
    double startY;
    double endX;
    double endY;
    double width;
    double height;
    
    public DragShapeTransaction(Draggable selectedDraggableShape, int x, int y, double startX, double startY, double endX, double endY, double width, double height){
        this.selectedDraggableShape = selectedDraggableShape;
        this.x = x;
        this.y = y;
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.width = width;
        this.height = height;
    }

    @Override
    public void doTransaction() {
        selectedDraggableShape.drag(x, y);
        selectedDraggableShape.setLocationAndSize(x, y, width, height);
        
    }

    @Override
    public void undoTransaction() {
        if(selectedDraggableShape instanceof DraggableRectangle)
            selectedDraggableShape.setLocationAndSize(endX, endY, width, height);
    }
    
}
